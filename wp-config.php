<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'WN2;0ZV9OZ/O*6JOyw4OciCU*HUGkHUC|`3]@7Zxl[i-`B;e/EPavs{FPgl#,RDD' );
define( 'SECURE_AUTH_KEY',  'b4%W&c%z[zh*V_:3OL$KbZ:4?=a!>!P!QO{y5|X$bd2XwzLz^w7@Fm:(>2CcFg/R' );
define( 'LOGGED_IN_KEY',    '!}h)U(8k,k|$SzG+4lA)ioV.%@vW]sk6S.VTwsCw^zAbiX*Z@Z4pwW7D~GVQYROB' );
define( 'NONCE_KEY',        '0o:NyY(>[h.4sY?6?kY1uMN$MreX@YCQFd!x^E3S8nqQ(~BqVAk)-s<S6A#6/n7d' );
define( 'AUTH_SALT',        'v/h1h0F.4PY}Z}dxFfpb&.C>vi^AWLR2O+{XQR%|Q]vswGzi!An-/VRGC4I@WI+g' );
define( 'SECURE_AUTH_SALT', ';jr`;{3;?unHsY&4Wlu1R3Emjc]A8xC!cr;K1)dUrRaCK@l,0N^?3}w;,<NF<Voo' );
define( 'LOGGED_IN_SALT',   'pS4.EI2:(WAor18cP]*6jkQ8@vffjPN;Cbn*1rSK63K@|zb@?*C)1Ie9iKl6T2e6' );
define( 'NONCE_SALT',       'I3|b}>:b>QO=|~?1BpZ,eLJ/a*9nN E6Lge)E&2g*Ja:iyrw2n:d2t,}Lo`z6qCI' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
